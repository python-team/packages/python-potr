Source: python-potr
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Hans-Christoph Steiner <hans@eds.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject,
 python3-all,
 python3-pycryptodome,
 python3-setuptools,
Standards-Version: 4.6.1
Homepage: https://github.com/python-otr/pure-python-otr
Vcs-Git: https://salsa.debian.org/python-team/packages/python-potr.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-potr
Testsuite: autopkgtest-pkg-python

Package: python3-potr
Architecture: all
Depends:
 python3-pycryptodome,
 ${misc:Depends},
 ${python3:Depends},
Provides:
 ${python3:Provides},
Breaks:
 python-potr,
Replaces:
 python-potr,
Description: Pure Python implementation of the OTR protocol (Python 3)
 pure-python-otr is a pure Python implementation of the OTR protocol
 (Off-the-Record encryption, see the OTR website for more
 information). This gives developers the flexibility to implement OTR
 encryption for their Python-based Instant Messaging clients. A
 gajim-otr plugin will be available in the next major Gajim release to
 provide IM encryption independently of the underlying chat protocol.
 .
 This package also includes the command line utility 'convertkey' for
 converting the old OTR file format to the new one.
 .
 This is the Python 3 version of the package.
