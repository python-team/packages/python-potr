python-potr (1.0.2-7) unstable; urgency=medium

  * Team upload.
  * debian/watch: Fix upstream version matching.

 -- Colin Watson <cjwatson@debian.org>  Sun, 20 Oct 2024 19:05:52 +0100

python-potr (1.0.2-6) unstable; urgency=medium

  * Team upload.
  * Disable tests, as this package currently doesn't have any (closes:
    #1080282).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Sep 2024 17:22:32 +0100

python-potr (1.0.2-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Re-export upstream signing key without extra signatures.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 21:33:00 +0100

python-potr (1.0.2-4) unstable; urgency=medium

  * d/changelog: release
  * d/control: add pycryptodome as Depends on package (Closes: 980473)
    - Add 0002-Use-of-Cryptodome-module-imports-instead-of-Crypto.patch
  * d/gbp.conf: Set debian/master as default branch.
  * d/control: Set autopkgtest.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Fri, 22 Jan 2021 22:37:37 +0100

python-potr (1.0.2-3) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Emmanuel Arias ]
  * d/control: Bump debhelper-compat to 13.
  * d/control: Build-Depends and Depends on python3-pycryptodome (Closes:
    #971306).
    - PyCrypto is no longer maintaned.
  * d/control: Bump Standards-Versions to 4.5.0 (from 4.4.1).
  * d/patches: add patch to remove the use of PyCrypto on
    setup.py as install_required.
  * d/salsa-ci.yml: update salsa-ci.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Wed, 30 Sep 2020 21:14:15 -0300

python-potr (1.0.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * Bump Standards-Version to 4.4.0.
  * d/control: Fix wrong Vcs-*.
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938041

 -- Sandro Tosi <morph@debian.org>  Thu, 23 Jan 2020 15:35:53 -0500

python-potr (1.0.2-1) unstable; urgency=medium

  * New upstream version
  * point Vcs tags to salsa.debian.org
  * update Homepage, old one is gone
  * modernize Build-Depends, debhelper 9
  * Standards-Version: 4.3.0 no changes

 -- Hans-Christoph Steiner <hans@eds.org>  Tue, 29 Jan 2019 22:48:24 +0000

python-potr (1.0.1-1.1) unstable; urgency=medium

  * Non-maintainer upload

  [ intrigeri ]
  * install the convertkey script in python-potr (Closes: #780610)
  * debian/control: don't pretend that python3-potr ships the convertkey script

  [ Hans-Christoph Steiner ]
  * purge copyright info about files that have been removed

 -- intrigeri <intrigeri@debian.org>  Mon, 25 May 2015 12:02:01 +0200

python-potr (1.0.1-1) unstable; urgency=low

  * new upstream version, 1.0.1

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 16 Oct 2014 17:49:14 -0400

python-potr (1.0.0-1) unstable; urgency=low

  * new upstream version, yay 1.0!
  * gajim-plugin source removed from upstream tarball

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 17 Oct 2013 17:27:04 -0400

python-potr (1.0.0~beta7-1) unstable; urgency=low

  * update to the latest upstream release
  * add python3 package
  * upload to unstable

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 23 Sep 2013 14:49:05 -0400

python-potr (1.0.0~beta6-1) experimental; urgency=low

  * Initial release (Closes: #651324)

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 20 Dec 2012 16:25:37 -0500
